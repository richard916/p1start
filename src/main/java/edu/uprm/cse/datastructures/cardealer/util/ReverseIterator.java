package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Iterator;

public class ReverseIterator<E> implements Iterator<E>{

	@Override
	public boolean hasNext() {
		return hasPrevious();
	}

	@Override
	public E next() {
		return previous();
	}

	public boolean hasPrevious() {
		return false;	
	}

	public E previous(){
		return null;	
	}

}
