package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node() {
			this.element = element;
			this.next = this.prev = null;
		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext(){
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev(){
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}
	private Node<E> header;
	private int currentSize;
	private Comparator<E> comparator;

	public CircularSortedDoublyLinkedList(Comparator <E> comp) {
		this.currentSize = 0;
		this.header = new Node<>();

		this.header.setNext(this.header);
		this.header.setPrev(this.header);

		this.comparator = comp;
	}

	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;

		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();

		}
		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			} 
			else {
				throw new NoSuchElementException(); 
			}
		}

	}

	private class CSDDLinkedListIndexIterator<E> implements Iterator<E>{
		private Node<E> nextNode;

		public CSDDLinkedListIndexIterator(int i) {
			this.nextNode = (Node<E>) header.getNext();
			int index = 0;
			while(index != i) {	nextNode = nextNode.getNext(); index++;	}
		}

		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		}

		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			} else throw new NoSuchElementException();
		}
	}

	private class CircularSortedDoublyLinkedListReverseIterator<E> extends ReverseIterator<E>{
		private Node<E> prevNode;

		public CircularSortedDoublyLinkedListReverseIterator() {
			this.prevNode = (Node<E>) header.getPrev();
		}

		@Override
		public boolean hasPrevious() {
			return prevNode.getElement() != null;
		}

		@Override
		public E previous() {
			if(this.hasPrevious()) {
				E result = this.prevNode.getElement();
				this.prevNode = this.prevNode.getPrev();
				return result;
			} else throw new NoSuchElementException();
		}

	}

	private class CSDDLinkedListReverseIndexIterator<E> extends ReverseIterator<E>{
		private Node<E> prevNode;

		public CSDDLinkedListReverseIndexIterator(int i) {
			this.prevNode = (Node<E>) header.getPrev();
			int index = currentSize-1;
			while(index != i) {	prevNode = prevNode.getPrev(); index--;	}
		}

		@Override
		public boolean hasPrevious() {
			return prevNode.getElement() != null;
		}

		@Override
		public E previous() {
			if(this.hasPrevious()) {
				E result = this.prevNode.getElement();
				this.prevNode = this.prevNode.getPrev();
				return result;
			} else throw new NoSuchElementException();
		}
	}

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	@Override
	public Iterator<E> iterator(int i) {
		return new CSDDLinkedListIndexIterator<E>(i);
	}

	@Override
	public ReverseIterator<E> reverseIterator() {
		return new CircularSortedDoublyLinkedListReverseIterator<E>();		
	}

	@Override
	public ReverseIterator<E> reverseIterator(int i) {
		return new CSDDLinkedListReverseIndexIterator<E>(i);
	}

	@Override
	public boolean add(E obj) {
		if(obj==null) return false;
		Node<E> newNode = new Node<E>();
		newNode.setElement(obj);

		if(this.isEmpty()) {
			this.header.setNext(newNode); this.header.setPrev(newNode);
			newNode.setNext(this.header); newNode.setPrev(this.header);
			this.currentSize++; return true;
		}
		boolean running = true;
		Node<E> temp = this.header.getNext();
		while(running) {

			if(this.comparator.compare(temp.getElement(), newNode.getElement()) == 0) {

				newNode.setPrev(temp.getPrev()); 
				newNode.setNext(temp);
				temp.getPrev().setNext(newNode);  
				temp.setPrev(newNode);
				this.currentSize++; 
				return true;

			} 
			else if ((this.comparator.compare(temp.getElement(), newNode.getElement()) < 0) 
					&& (temp.getNext().getElement()==null)) {

				newNode.setPrev(temp); 
				newNode.setNext(this.header);
				this.header.setPrev(newNode);  
				temp.setNext(newNode);
				this.currentSize++; 
				return true;

			} 
			else if ((this.comparator.compare(temp.getElement(), newNode.getElement()) > 0) 
					&& (temp.getPrev().getElement()==null)) {

				newNode.setNext(temp);  
				newNode.setPrev(this.header);
				this.header.setNext(newNode); 
				temp.setPrev(newNode);
				this.currentSize++;  
				return true;

			}
			else if ((this.comparator.compare(temp.getElement(), newNode.getElement()) < 0) 
					&& (this.comparator.compare(temp.getNext().getElement(), newNode.getElement()) > 0)) {

				newNode.setPrev(temp);  
				newNode.setNext(temp.getNext());
				temp.getNext().setPrev(newNode); 
				temp.setNext(newNode);
				this.currentSize++; 
				return true;

			} 
			else if (temp.getNext().getElement()==null) {

				newNode.setPrev(temp);  
				newNode.setNext(this.header);
				temp.setNext(newNode);  
				this.header.setPrev(newNode);
				this.currentSize++; 
				return true;

			}
			temp = temp.getNext();

		}	return false;

	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {

		if(obj == null) {
			return false;
		}

		Node<E> newNode= this.header.getNext();

		int counter = 0;

		while(counter<this.currentSize) {
			if(this.comparator.compare(newNode.getElement(), obj) == 0) {
				newNode.getPrev().setNext(newNode.getNext());
				newNode.getNext().setPrev(newNode.getPrev());
				newNode.setElement(null);
				newNode.setNext(null);
				newNode.setPrev(null);
				this.currentSize--;
				return true;
			}else {
				newNode = newNode.getNext();
				counter++;
			}
		}
		return false;
	}

	@Override
	public boolean remove(int index) {
		if(index < 0 || index >= this.currentSize) {
			throw new IndexOutOfBoundsException("Index is incorrect");
		}
		else {
			Node<E> newNode = this.header.getNext();
			int counter = 0;

			while(counter != index) {
				newNode = newNode.getNext();
				counter++;
			}
			newNode.getPrev().setNext(newNode.getNext());
			newNode.getNext().setPrev((newNode.getPrev()));
			newNode.setElement(null);
			newNode.setNext(null);
			newNode.setPrev(null);
			this.currentSize--;
			return true;	
		}


	}

	@Override
	public int removeAll(E obj) {
		int counter = 0;
		while(this.remove(obj)) {
			counter++;
		}
		return counter;
	}

	@Override
	public E first() {
		return get(0);
	}

	@Override
	public E last() {
		return this.get(this.currentSize-1);
	}

	@Override
	public E get(int index) {		
		if(index < 0 || this.currentSize <= index) {
			throw new IndexOutOfBoundsException("Index is not found on the list");
		}

		int currentPos = 0;
		Node<E> temp = this.header.getNext();

		while(currentPos != index) {
			temp = temp.getNext();
			currentPos++;
		}
		return temp.getElement();
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;		
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize == 0;
	}

	@Override
	public int firstIndex(E e) {
		Node<E> temp  = this.header.getNext();
		int currentPos = 0;
		while(this.currentSize > currentPos) {
			if(this.comparator.compare(temp.getElement(), e)!= 0) {
				temp = temp.getNext();
				currentPos++;
			}else {
				return currentPos;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		Node<E> temp = this.header.getPrev();
		int counter = this.currentSize-1;
		while(counter >= 0) {
			if(this.comparator.compare(temp.getElement(), e) != 0) {
				counter--;
				temp = temp.getPrev();
			}else {
				return counter;
			}
		}
		return -1;
	}

}
