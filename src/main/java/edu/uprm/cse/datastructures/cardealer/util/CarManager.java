package edu.uprm.cse.datastructures.cardealer.util;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import edu.uprm.cse.datastructures.cardealer.model.*;
import edu.uprm.cse.datastructures.cardealer.CarList;

@Path("/cars")
public class CarManager {

	private static CircularSortedDoublyLinkedList<Car> cList = CarList.getList();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		if(cList.isEmpty()) return new Car[0];
		Car[] result = new Car[cList.size()];
		int i = 0;
		while (i < result.length) { 
			result[i] = cList.get(i); 
			i++; 
		}
		return result;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		if(!cList.isEmpty()) {
			for (int i = 0; i < cList.size(); i++) {
				if (cList.get(i).getCarId() == id) {
					return cList.get(i);
				}
			} 
		}
		throw new WebApplicationException(404);	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		cList.add(car);
		return Response.status(201).build();
	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		for (int i = 0; i < cList.size(); i++) {
			if (cList.get(i).getCarId() == car.getCarId()) {
				cList.remove(i);	
				cList.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}

	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {
		for (int i = 0; i < cList.size(); i++) {
			if (cList.get(i).getCarId() == id) {
				cList.remove(i);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
}
